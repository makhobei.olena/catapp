
import Foundation
import UIKit


class NetworkSession{
   static let shared = NetworkSession()
    
    let baseURL = URL(string:"https://aws.random.cat/meow")!
    
    func fetchCatInfo (completion: @escaping (Result<CatModel?,CatError>) -> Void) {
        let url = baseURL
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
               let catInfo : CatModel = try?
                jsonDecoder.decode(CatModel.self, from: data)
            {
                completion(.success(catInfo))
            }
            else {
                completion(.failure(.noDataAvailable))
            }
        }
        dataTask.resume()
    }
    
    enum CatError :Error
    {
        case noDataAvailable
    }
}
