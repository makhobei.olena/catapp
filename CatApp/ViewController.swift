import UIKit
//savethis

class ViewController: UIViewController {
    var pathName : String = ""
    
    @IBOutlet var catImageView: UIImageView!
    
    @IBOutlet var catLabel: UILabel!
    
    @IBOutlet var catButton: UIButton!
    
    @IBAction func catButtonPressed(_ sender: Any) {
        self.catButton.isEnabled = false
        self.catLogButton.isEnabled = false
        self.catLabel.isEnabled = false
        self.loadData()
    }
    
    @IBAction func catLogButtonPressed(_sender:Any){
        print("Name of file: \(pathName)")
    }
    
    @IBOutlet var catLogButton: UIButton!
    
    func loadData(){
        NetworkSession.shared.fetchCatInfo{ result in
            DispatchQueue.main.async{
                if let catInfo = try?result.get() {
                    self.updateUI(with: catInfo)
                }else {
                    let alert = UIAlertController(title: "Error", message: "Failed to loading data,please, try again later", preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.catLabel.isEnabled = true
                    self.catButton.isEnabled = true
                    self.catLogButton.isEnabled = true
                    
                    print("Error occured")
                }
            }
        }
        
    }
    
    //this would be update Image
    func updateUI(with catInfo: CatModel) {
        let task = URLSession.shared.dataTask(with: catInfo.file , completionHandler: { (data, response, error) in
            if let data = data,
               let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.catImageView.image = image
                    self.pathName = catInfo.file.lastPathComponent
                    self.catLabel.text = self.pathName
                    self.catButton.isEnabled = true
                    self.catLogButton.isEnabled = true
                    self.catLabel.isEnabled = true
                }
            }
        })
        task.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        catButton.layer.cornerRadius = 15
        catLabel.layer.masksToBounds = true
        catLabel.layer.cornerRadius = 15
        catLogButton.layer.cornerRadius = 15
        catImageView.layer.cornerRadius = 15
    }
    
}

extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}






